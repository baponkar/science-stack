package com.baponkar.sciencestack;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class StackViewAdapter extends ArrayAdapter {
    private List<String> storesList;
    private Context context;
    private int itemLayout;

    private OnChildItemClickListener mListener; // Add this member
    public StackViewAdapter(List<String> stores, int resource, Context ctx,OnChildItemClickListener listener){
        super(ctx, resource, stores);
        storesList = stores;
        context = ctx;
        itemLayout = resource;
        mListener = listener;
    }

    @Override
    public int getCount(){
        return storesList.size();
    }

    @Override
    public String getItem(int position){
        return storesList.get(position);
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent){
        if(view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        }
        String store = storesList.get(position);

        TextView storeName = view.findViewById(R.id.text);
        ImageView storeImage = view.findViewById(R.id.image);

        storeName.setText(store);

        int resId = context.getResources().getIdentifier(store,"drawable", context.getPackageName());
        storeImage.setImageResource(resId);

        storeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onChildItemClick(position); // Notify the activity
                }
                // Handle click event for the child view

            }
        });

        return view;
    }

    public interface OnChildItemClickListener {
        void onChildItemClick(int position);
    }

}
