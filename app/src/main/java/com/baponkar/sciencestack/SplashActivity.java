package com.baponkar.sciencestack;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import org.json.JSONArray;

public class SplashActivity extends AppCompatActivity {
    private static final int splashTime = 2500;


    ImageView imageView;
    SharedPreferences pref;
    private static final String PREF_URLS = "urls";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //hiding title bar
        getSupportActionBar().hide();

        Animation animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoom_in);

        imageView = findViewById(R.id.imageView);
        imageView.setVisibility(View.VISIBLE);
        imageView.setAnimation(animZoomIn);

        TextView title = findViewById(R.id.textView);
        title.startAnimation(animZoomIn);

        pref = getApplicationContext().getSharedPreferences("Settings", 0 );
        final SharedPreferences.Editor editor = pref.edit();


        Thread welcomeThread = new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(splashTime);  //Delay of 3 seconds
                } catch (Exception e) {

                } finally {
                    if (pref.getBoolean("firstRun", true)) {
                        //Initialize Privacy accepting Dialog
                        Intent intent = new Intent(SplashActivity.this, AcceptActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        //loading main activity
                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }
        };
        welcomeThread.start();
    }

    private void saveUrlsToPreferences(String[] urls) {
        JSONArray jsonArray = new JSONArray();
        for (String url : urls) {
            jsonArray.put(url);
        }
        pref.edit()
                .putString(PREF_URLS, jsonArray.toString())
                .apply();
    }

}