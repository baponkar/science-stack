package com.baponkar.sciencestack;

import android.content.Context;
import android.content.res.Resources;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class JsonReader {

    public static JSONArray getArray(Context context, String  heading) {
        try {
            Resources resources = context.getResources();
            InputStream inputStream = resources.openRawResource(R.raw.science_url);
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            inputStream.close();

            String jsonContent = new String(buffer, StandardCharsets.UTF_8);
            JSONObject jsonObject = new JSONObject(jsonContent);
            JSONObject scienceUrlObject = jsonObject.getJSONObject("SCIENCE_URL");

            return scienceUrlObject.getJSONArray(heading);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}

