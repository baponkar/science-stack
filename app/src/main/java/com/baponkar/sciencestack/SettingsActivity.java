package com.baponkar.sciencestack;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    ListView listView;
    private ActionBarDrawerToggle toggle;

    private EditText editText;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        listView = findViewById(R.id.listView);
        editText = findViewById(R.id.editText);

        pref = getApplicationContext().getSharedPreferences("saved urls", 0 );
        editor = pref.edit();

        final ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("Removing an URL");
        arrayList.add("Reset");
        arrayList.add("Adding new url");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,arrayList);

        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        // pass the Open and Close toggle for the drawer layout listener
        // to toggle the button
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // Handle navigation drawer item clicks here
                switch (item.getItemId()) {
                    case R.id.home:
                        startActivity(new Intent(SettingsActivity.this, MainActivity.class));
                        break;
                    case R.id.saved_page:
                        startActivity(new Intent(SettingsActivity.this, SaveActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_settings:
                        Toast.makeText(SettingsActivity.this, "settings", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SettingsActivity.this, SettingsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_about:
                        //Toast.makeText(MainActivity.this, "About", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SettingsActivity.this, AboutActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.terms:
                        startActivity(new Intent(SettingsActivity.this, TermsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.privacy:
                        startActivity(new Intent(SettingsActivity.this, PrivacyActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_exit:
                        //Toast.makeText(MainActivity.this, "exit", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        String url = editText.getText().toString();

        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                //ListView clicked item index
                int index = position;
                switch(index){
                    case 0:
                        Log.d("msg", String.valueOf(index));
                    case 1:
                        Log.d("msg", String.valueOf(index));
                    case 2:
                        if(URLUtil.isValidUrl(editText.getText().toString()))

                        Toast.makeText(SettingsActivity.this, "Not a valid url!", Toast.LENGTH_SHORT).show();

                        Log.d("msg", editText.getText().toString());
                }

            }
        });
    }

    // override the onOptionsItemSelected()
    // function to implement
    // the item click listener callback
    // to open and close the navigation
    // drawer when the icon is clicked
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}