package com.baponkar.sciencestack;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.widget.StackView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements StackViewAdapter.OnChildItemClickListener{
    StackView stackView;
    String heading = "";
    Intent intent;
    String TAG = "MainActivity";


    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    private TextView quoteTextView;
    private List<String> quotes = new ArrayList<>();
    private int currentQuoteIndex = 0;
    private Handler handler = new Handler();
    private Timer autoFlipTimer;

    private TextView authorTextView;
    private List<String> authors = new ArrayList<>();

    private TextView factTextView;
    private List<String> facts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        quoteTextView = findViewById(R.id.quoteTextView);
        authorTextView = findViewById(R.id.authorTextView);

        factTextView = findViewById(R.id.factTextView);

        // Fetch JSON data and populate the quotes list
        fetchQuoteDataFromUrl();

        //Fetch JSON data and populate the facts list
        //fetchFactDataFromUrl();

        // Initialize the timer to auto-flip quotes every 5 seconds
        autoFlipTimer = new Timer();
        autoFlipTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                flipToNextQuote();
            }
        }, 5000, 5000);



        intent = new Intent(this, FragmentActivity.class);

        // drawer layout instance to toggle the menu icon to open
        // drawer and back button to close drawer
        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        // pass the Open and Close toggle for the drawer layout listener
        // to toggle the button
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // Handle navigation drawer item clicks here
                switch (item.getItemId()) {
                    case R.id.home:
                        startActivity(new Intent(MainActivity.this, MainActivity.class));
                        break;
                    case R.id.saved_page:
                        startActivity(new Intent(MainActivity.this, SaveActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_settings:
                        Toast.makeText(MainActivity.this, "settings", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_about:
                        //Toast.makeText(MainActivity.this, "About", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this, AboutActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.terms:
                        startActivity(new Intent(MainActivity.this, TermsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.privacy:
                        startActivity(new Intent(MainActivity.this, PrivacyActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_exit:
                        //Toast.makeText(MainActivity.this, "exit", Toast.LENGTH_SHORT).show();
                        finish();
                        break;

                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }

        });

        stackView = findViewById(R.id.stackView);
        StackViewAdapter albumsAdapter = new StackViewAdapter(getStores(), R.layout.item, MainActivity.this, this);
        stackView.setAdapter(albumsAdapter);

    }

    // override the onOptionsItemSelected()
    // function to implement
    // the item click listener callback
    // to open and close the navigation
    // drawer when the icon is clicked
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //manage StackView item
    @Override
    public void onChildItemClick(int position) {
        // Handle the click event for the child item at the given position
        // You can use the position to access data or perform actions related to the clicked item
        Log.d(TAG, String.valueOf(position));
        switch(position){
            case 0:
                heading = getResources().getString(R.string.physics);
                break;
            case 1:
                heading = getResources().getString(R.string.math);
                break;
            case 2:
                heading = getResources().getString(R.string.chemistry);
                break;
            case 3:
                heading = getResources().getString(R.string.medical);
                break;
            case 4:
                heading = getResources().getString(R.string.astronomy);
                break;
            case 5:
                heading = getResources().getString(R.string.biology);
                break;
            case 6:
                heading = getResources().getString(R.string.psychology);
                break;
            case 7:
                heading = getResources().getString(R.string.neuroscience);
                break;
            case 8:
                heading = getResources().getString(R.string.computer);
                break;
            case 9:
                heading = getResources().getString(R.string.technology);
                break;
            case 10:
                heading = getResources().getString(R.string.environment);
                break;
            case 11:
                heading = getResources().getString(R.string.other);
                break;
            case 12:
                heading = getResources().getString(R.string.blog);
                break;
            case 13:
                heading = getResources().getString(R.string.earth);
                break;
            case 14:
                heading = getResources().getString(R.string.archeology);
                break;
            case 15:
                heading = getResources().getString(R.string.news);
                break;

        }

        //sending which type news opening in FragmentActivity
        intent.putExtra("heading", heading);
        startActivity(intent);
    }

    private List<String> getStores(){
        List<String> stores = new ArrayList<String>();
        stores.add("physics");
        stores.add("mathematics");
        stores.add("chemistry");
        stores.add("medical");
        stores.add("astronomy");
        stores.add("biology");
        stores.add("psychology");
        stores.add("neuroscience");
        stores.add("computer");
        stores.add("technology");
        stores.add("environment");
        stores.add("other");
        stores.add("blog");
        stores.add("earth");
        stores.add("archaeology");
        stores.add("news");

        return stores;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Cancel the timer when the activity is destroyed
        if (autoFlipTimer != null) {
            autoFlipTimer.cancel();
        }
    }
    private void fetchQuoteDataFromUrl() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://type.fit/api/quotes")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String responseData = response.body().string();
                        parseJsonData(responseData);
                        updateQuoteTextView();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void parseJsonData(String jsonData) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonData);
        quotes.clear();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String text = jsonObject.getString("text");
            String author = jsonObject.getString("author");
            quotes.add(text);
            authors.add(author);
        }
    }

    private void updateQuoteTextView() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!quotes.isEmpty()) {
                    quoteTextView.setText(quotes.get(currentQuoteIndex));
                }
                if(!authors.isEmpty()){
                    authorTextView.setText("- " + authors.get(currentQuoteIndex));
                }
            }
        });
    }

    private void flipToNextQuote() {
        currentQuoteIndex = (currentQuoteIndex + 1) % quotes.size();
        updateQuoteTextView();
    }

    public static boolean checkNetworkConnection(Context _context) {
        //If there is net connection then this method
        //will give true otherwise give false
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;
    }

}

