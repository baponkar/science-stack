package com.baponkar.sciencestack;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class AcceptActivity extends AppCompatActivity {
    Button privacyBtn, termsBtn, proccedBtn, cancelBtn;
    CheckBox checkBox;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept);


        privacyBtn = findViewById(R.id.privacyBtn);
        privacyBtn = findViewById(R.id.termsBtn);
        privacyBtn = findViewById(R.id.proceedBtn);
        privacyBtn = findViewById(R.id.cancelBtn);

        checkBox = findViewById(R.id.checkbox);

        pref = getApplicationContext().getSharedPreferences("Settings", 0 );


    }

    public void onClick(View view){
        switch(view.getId()){
            case R.id.privacyBtn:{
                //Initialize Privacy accepting Dialog
                Intent intent = new Intent(AcceptActivity.this, PrivacyActivity.class);
                startActivity(intent);
            }
            break;

            case R.id.termsBtn:{
                //Initialize Privacy accepting Dialog
                Intent intent = new Intent(AcceptActivity.this, TermsActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.proceedBtn:{
                if(checkBox.isChecked()){
                    // Do first run stuff here then set 'firstrun' as false
                    pref.edit().putInt("homeIndex", 0).commit(); //initialize homeIndex into zero
                    // using the following line to edit/commit prefs
                    pref.edit().putBoolean("firstRun", false).commit();
                    //Initialize Main Dialog
                    Intent intent = new Intent(AcceptActivity.this, IntroActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(this, "You need to accept terms and condition checkbox.", Toast.LENGTH_SHORT).show();
                }
                break;

            }
            case R.id.cancelBtn:Btn:{
                //Close the app
                super.finish();
                this.finishAffinity();
            }
                break;
        }
    }
}