package com.baponkar.sciencestack;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class FragmentActivity extends AppCompatActivity {
    ViewPager2Adapter adapter;
    ViewPager2 viewPager;

    Context context = FragmentActivity.this;

    public  List<String> savedUrlList = new ArrayList<String>();
    String heading;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        getSupportActionBar().show();
        // drawer layout instance to toggle the menu icon to open
        // drawer and back button to close drawer
        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        // pass the Open and Close toggle for the drawer layout listener
        // to toggle the button
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // Handle navigation drawer item clicks here
                switch (item.getItemId()) {
                    case R.id.home:
                        startActivity(new Intent(context, MainActivity.class));
                        break;
                    case R.id.saved_page:
                        startActivity(new Intent(context, SaveActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_settings:
                        Toast.makeText(context, "settings", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(context, SettingsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_about:
                        //Toast.makeText(MainActivity.this, "About", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(context, AboutActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.terms:
                        startActivity(new Intent(context, TermsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.privacy:
                        startActivity(new Intent(context, PrivacyActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_exit:
                        //Toast.makeText(MainActivity.this, "exit", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }

        });



        viewPager = findViewById(R.id.viewPager);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //received heading type from MainActivity
            heading = extras.getString("heading");
        }
        List<String> savedUrlList = GetSavedUrlList("saveUrls", this, "SAVED URL");
        adapter = new ViewPager2Adapter(this, GetDefaultUrls(heading), savedUrlList);
        viewPager.setAdapter(adapter);


    }

    // override the onOptionsItemSelected()
    // function to implement
    // the item click listener callback
    // to open and close the navigation
    // drawer when the icon is clicked
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onBackPressed() {
        // Call the goBack method in WebViewPagerAdapter to handle WebView history navigation
        if (adapter != null && viewPager != null) {
            if(adapter.goBack())
            {

            }
            else {
                finish();
            }
        } else {
            super.onBackPressed();
            finish();
        }
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    List<String> GetDefaultUrls(String heading)
    {
        //This method will load deafault urls by using heading from json file
        List<String> urlList = new ArrayList<>();
        //String heading = getIntent().getExtras().getString("heading");

        JSONArray array = JsonReader.getArray(this,heading );
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                try {
                    String Url = array.getString(i);
                    urlList.add(Url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return urlList;
    }

    List<String> GetSavedUrlList(String SHARED_PREFS, Context context, String KEY )
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String csvList = sharedPreferences.getString(KEY, "");
        String[] items = csvList.split(",");
        List<String> list = new ArrayList<String>();
        for(int i=0; i < items.length; i++){
            list.add(items[i]);
        }
        return list;
    }
}
