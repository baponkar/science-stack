package com.baponkar.sciencestack;



import android.content.Intent;
import android.os.Bundle;

import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughActivity;
import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughCard;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends FancyWalkthroughActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_intro);


        FancyWalkthroughCard fancywalkthroughCard1 = new FancyWalkthroughCard("Welcome!", "This is a short tutorial how to use.", R.drawable.science_stack_icon_1920_1920);
        fancywalkthroughCard1.setBackgroundColor(R.color.white);
        fancywalkthroughCard1.setTitleColor(R.color.black);
        fancywalkthroughCard1.setDescriptionColor(R.color.black);
        //fancywalkthroughCard1.setDisplaySkip(false);
        //fancywalkthroughCard1.setTitleTextSize(dpToPixels(10, this));
        //fancywalkthroughCard1.setDescriptionTextSize(dpToPixels(8, this));
        //fancywalkthroughCard1.setIconLayoutParams(iconWidth, iconHeight, marginTop, marginLeft, marginRight, marginBottom);

        FancyWalkthroughCard fancywalkthroughCard2 = new FancyWalkthroughCard("Main Menu", "You can choose category by swiping up or down.", R.drawable.screenshot_main_menu);
        fancywalkthroughCard1.setBackgroundColor(R.color.white);
        fancywalkthroughCard1.setTitleColor(R.color.black);
        fancywalkthroughCard1.setDescriptionColor(R.color.black);

        FancyWalkthroughCard fancywalkthroughCard3 = new FancyWalkthroughCard("Change page", "Change webpage by swipe left right.", R.drawable.screenshot_page);
        fancywalkthroughCard1.setBackgroundColor(R.color.white);
        fancywalkthroughCard1.setTitleColor(R.color.black);
        fancywalkthroughCard1.setDescriptionColor(R.color.black);

        FancyWalkthroughCard fancywalkthroughCard4 = new FancyWalkthroughCard("Save page", "Save the current open page which can be seen by Saved Page option from navigation drawer.", R.drawable.screenshot_page_2);
        fancywalkthroughCard1.setBackgroundColor(R.color.white);
        fancywalkthroughCard1.setTitleColor(R.color.black);
        fancywalkthroughCard1.setDescriptionColor(R.color.black);

        List<FancyWalkthroughCard> pages = new ArrayList<>();
        pages.add(fancywalkthroughCard1);
        pages.add(fancywalkthroughCard2);
        pages.add(fancywalkthroughCard3);
        pages.add(fancywalkthroughCard4);


        setOnboardPages(pages);

        setImageBackground(R.drawable.science_stack_icon_1920_1920);
    }

    @Override
    public void onFinishButtonPressed() {
        startActivity(new Intent(IntroActivity.this, MainActivity.class));
    }
}