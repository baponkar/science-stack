package com.baponkar.sciencestack;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class SaveActivity extends AppCompatActivity {
    ViewPager2Adapter adapter;
    ViewPager2 viewPager;

    public  List<String> savedUrlList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

        getSupportActionBar().hide();

        viewPager = findViewById(R.id.viewPager);

        String heading = "PHYSICS"; //setting default as "PHYSICS"
        List<String> savedUrlList = GetSavedUrlList("saveUrls", this, "SAVED URL");
        adapter = new ViewPager2Adapter(this, GetUrls(heading), savedUrlList);
        viewPager.setAdapter(adapter);


    }

    @Override
    public void onBackPressed() {
        // Call the goBack method in WebViewPagerAdapter to handle WebView history navigation
        if (adapter != null && viewPager != null) {
            if(adapter.goBack())
            {
                    //do nothing in activity
            }
            else {
                finish();
            }
        } else {
            super.onBackPressed();
            finish();
        }
    }

    List<String> GetUrls(String heading)
    {
        List<String> urlList = new ArrayList<>();
        //String heading = getIntent().getExtras().getString("heading");

        JSONArray array = JsonReader.getArray(this,heading );
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                try {
                    String Url = array.getString(i);
                    urlList.add(Url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return urlList;
    }

    List<String> GetSavedUrlList(String SHARED_PREFS, Context context, String KEY )
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String csvList = sharedPreferences.getString(KEY, "");
        String[] items = csvList.split(",");
        List<String> list = new ArrayList<String>();
        for(int i=0; i < items.length; i++){
            list.add(items[i]);
        }
        return list;
    }
}
