package com.baponkar.sciencestack;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

public class AboutActivity extends AppCompatActivity  implements StackViewAdapter.OnChildItemClickListener{

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    Intent saveIntent;
    Intent homeIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        saveIntent = new Intent(this, SaveActivity.class);
        homeIntent = new Intent(this, MainActivity.class);

        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        // pass the Open and Close toggle for the drawer layout listener
        // to toggle the button
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // Handle navigation drawer item clicks here
                switch (item.getItemId()) {
                    case R.id.home:
                        startActivity(new Intent(AboutActivity.this, MainActivity.class));
                        break;
                    case R.id.saved_page:
                        startActivity(new Intent(AboutActivity.this, SaveActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_settings:
                        Toast.makeText(AboutActivity.this, "settings", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AboutActivity.this, SettingsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_about:
                        //Toast.makeText(MainActivity.this, "About", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AboutActivity.this, AboutActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.terms:
                        startActivity(new Intent(AboutActivity.this, TermsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.privacy:
                        startActivity(new Intent(AboutActivity.this, PrivacyActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_exit:
                        //Toast.makeText(MainActivity.this, "exit", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }

        });
    }

    @Override
    public void onChildItemClick(int position) {

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}