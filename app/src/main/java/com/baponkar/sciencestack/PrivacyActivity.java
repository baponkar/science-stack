package com.baponkar.sciencestack;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.navigation.NavigationView;

public class PrivacyActivity extends AppCompatActivity {
    WebView webView;
    LottieAnimationView loadingAnimationView;

    SharedPreferences pref;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

        pref = getApplicationContext().getSharedPreferences("Settings", 0 );

        webView = findViewById(R.id.webView);
        loadingAnimationView = findViewById(R.id.loadingAnimationView);

        loadUrl("https://baponkar.github.io/Science-News/privacy.html");
        // drawer layout instance to toggle the menu icon to open
        // drawer and back button to close drawer
        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        // pass the Open and Close toggle for the drawer layout listener
        // to toggle the button
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        if(pref.getBoolean("firstRun", true))
        {
            //hiding navigation drawer if first run
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        else{
            // to make the Navigation drawer icon always appear on the action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }



        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // Handle navigation drawer item clicks here
                switch (item.getItemId()) {
                    case R.id.home:
                        startActivity(new Intent(PrivacyActivity.this, MainActivity.class));
                        break;
                    case R.id.saved_page:
                        startActivity(new Intent(PrivacyActivity.this, SaveActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_settings:
                        Toast.makeText(PrivacyActivity.this, "settings", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(PrivacyActivity.this, SettingsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_about:
                        //Toast.makeText(MainActivity.this, "About", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(PrivacyActivity.this, AboutActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.terms:
                        startActivity(new Intent(PrivacyActivity.this, TermsActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.privacy:
                        startActivity(new Intent(PrivacyActivity.this, PrivacyActivity.class));
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        break;
                    case R.id.nav_exit:
                        //Toast.makeText(MainActivity.this, "exit", Toast.LENGTH_SHORT).show();
                        finish();
                        break;

                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }

        });

    }

    // override the onOptionsItemSelected()
    // function to implement
    // the item click listener callback
    // to open and close the navigation
    // drawer when the icon is clicked
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Handel Back Button Pressed
    public void onBackPressed() {

        finish();
    }

    public void loadUrl(String url){
        if(checkNetworkConnection(this))
        {
            webView.loadUrl(url);
        }
        else{
            webView.loadUrl("file:///android_res/raw/error.html");
        }
    }
    public static boolean checkNetworkConnection(Context _context){
        //If there is net connection then this method
        //will give true otherwise give false
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;
    }
    public class WebViewClient extends android.webkit.WebViewClient{
        //To handel webview connection
        @Override
        public void onPageStarted(WebView webView, String url, Bitmap favicon){
            super.onPageStarted(webView,url,favicon);
            loadingAnimationView.setVisibility(View.VISIBLE);
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView webView,String url){
            webView.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView webView, String url){
            super.onPageFinished(webView, url);
            loadingAnimationView.setVisibility(View.GONE);
        }

        @Override
        public void onLoadResource(WebView webView, String url){
            //Todo auto-generated method sub
            super.onLoadResource(webView, url);
        }

        public void OnReceivedError(WebView webView,int errorCode, String description, String failedUrl){
            super.onReceivedError(webView,errorCode,description,failedUrl);

            if(errorCode == -2) {
                webView.loadUrl("file:///android_res/raw/error.html");
            }
        }
    }
}