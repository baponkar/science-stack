package com.baponkar.sciencestack;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class ViewPager2Adapter extends RecyclerView.Adapter<ViewPager2Adapter.ViewHolder> {
    WebView webView;
    static LottieAnimationView loadingAnimationView;
    static LottieAnimationView noInternet;
    static  FloatingActionButton shareButton;
    static FloatingActionButton bookmarkButton;
    static FloatingActionButton deleteButton;
    private List<String> urls;
    private List<String> savedUrlList;
    private Context context;

    public ViewPager2Adapter(Context context, List<String> urls, List<String> savedUrlList) {
        this.context = context;
        this.urls = urls;
        this.savedUrlList = savedUrlList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewpager2_item, parent, false);

        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        webView = holder.webView;
        shareButton = holder.shareButton;
        bookmarkButton = holder.bookmarkButton;
        deleteButton = holder.deleteButton;

        loadingAnimationView = holder.loadingAnimationView;
        noInternet = holder.noInternet;

        if(checkNetworkConnection(context))
        {
            String url = urls.get(position);
            holder.webView.loadUrl(url);
        }
        else
        {
            noInternet.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return urls.size();
    }

    public boolean goBack() {
        if (webView != null & webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        else {
            return false;
        }
    }


    public static boolean checkNetworkConnection(Context _context) {
        //If there is net connection then this method
        //will give true otherwise give false
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        WebView webView;
        LottieAnimationView loadingAnimationView;
        LottieAnimationView noInternet;
        FloatingActionButton shareButton;
        FloatingActionButton bookmarkButton;
        FloatingActionButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            webView = itemView.findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebChromeClient(new WebChromeClient());
            webView.setWebViewClient(new WebViewClient());

            loadingAnimationView = itemView.findViewById(R.id.loadingAnimationView);
            loadingAnimationView.setVisibility(View.GONE);

            noInternet = itemView.findViewById(R.id.noInternetConnection);
            noInternet.setVisibility(View.GONE);

            shareButton = itemView.findViewById(R.id.shareButton);
            bookmarkButton = itemView.findViewById(R.id.bookmarkButton);
            deleteButton = itemView.findViewById(R.id.deleteButton);
            deleteButton.setVisibility(View.GONE);

            if(context.getClass() == FragmentActivity.class)
            {
                bookmarkButton.setVisibility(View.VISIBLE);
                deleteButton.setVisibility(View.GONE);
            }
            else
            {
                deleteButton.setVisibility(View.VISIBLE);
                bookmarkButton.setVisibility(View.GONE);
            }

            bookmarkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //saving url in shared preference
                    Log.d("============>", "Bookmark Button Works!");
                    Toast.makeText(context, webView.getUrl() + " saved!", Toast.LENGTH_SHORT).show();
                    SaveUrl(webView.getUrl());
                }
            });
            shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //saving url in shared preference
                    Log.d("============>", "Share Button Works!");

                    String currentUrl = webView.getUrl();

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, currentUrl);
                    sendIntent.setType("text/plain");

                    Intent shareIntent = Intent.createChooser(sendIntent, null);
                    context.startActivity(shareIntent);
                }
            });
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //saving url in shared preference
                    Log.d("============>", "Delete Button Works!");
                    Toast.makeText(context, webView.getUrl() + " deleted!", Toast.LENGTH_SHORT).show();
                    DeleteUrl(webView.getUrl());
                }
            });

        }




        void SaveUrl(String url)
        {
            Boolean saved = false;
            for(int i=0;i<savedUrlList.size();i++)
            {
                if(savedUrlList.contains(url))
                {
                    saved = true;
                }
            }

            if(!saved)
            {
                SharedPreferences sharedPreferences = context.getSharedPreferences("saveUrls", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                savedUrlList.add(url);
                StringBuilder csvList = new StringBuilder();
                for(String s : savedUrlList){
                    csvList.append(s);
                    csvList.append(",");
                }
                editor.putString("SAVED URL", csvList.toString());
                editor.commit();
            }
        }
    }


    void DeleteUrl(String url)
    {
        Boolean have = false;
        for(int i=0;i<savedUrlList.size();i++)
        {
            if(savedUrlList.contains(url))
            {
                have = true;
            }
        }

        if(!have)
        {
            SharedPreferences sharedPreferences = context.getSharedPreferences("saveUrls", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            savedUrlList.remove(url);
            StringBuilder csvList = new StringBuilder();
            for(String s : savedUrlList){
                csvList.append(s);
                csvList.append(",");
            }
            editor.putString("SAVED URL", csvList.toString());
            editor.commit();
        }
    }


    public class WebViewClient extends android.webkit.WebViewClient {
        //To handel webview connection
        @Override
        public void onPageStarted(WebView webView, String url, Bitmap favicon) {
            super.onPageStarted(webView, url, favicon);
            loadingAnimationView.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            webView.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView webView, String url) {
            CookieSyncManager.getInstance().sync();
            super.onPageFinished(webView, url);
            loadingAnimationView.setVisibility(View.GONE);
        }

        @Override
        public void onLoadResource(WebView webView, String url) {
            //Todo auto-generated method sub
            super.onLoadResource(webView, url);
        }

        public void OnReceivedError(WebView webView, int errorCode, String description, String failedUrl) {
            super.onReceivedError(webView, errorCode, description, failedUrl);
            if (errorCode == -2) {
                //Loading Error HTML Page
                // webView.loadUrl("file:///android_res/raw/error.html");
                noInternet.setVisibility(View.VISIBLE);
            }
        }
    }
}

